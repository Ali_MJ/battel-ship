import os

ground1 = [[0 for j in range(0,10)]for i in range(0,10)]
ground2 = [[0 for j in range(0,10)]for i in range(0,10)]
shownground1 = [[0 for j in range(0,10)]for i in range(0,10)]
shownground2 = [[0 for j in range(0,10)]for i in range(0,10)]
def _place_ships():
	os.system('clear')
	print("Player 1 Form Your Ground: ")
	print("")

	a = int(input("Enter column for 2*1 ship: "))
	b = list(map(int,input("Enter Rows for 2*1 ship: ").strip().split(" ")))
	for i in b:
		ground1[i][a] = 1 

	a = int(input("Enter Row for 1*2 ship: "))
	b = list(map(int,input("Enter columns for 1*2 ship: ").strip().split(" ")))
	for i in b:
		ground1[a][i] = 1

	a = int(input("Enter column for 3*1 ship: "))
	b = list(map(int,input("Enter Rows for 3*1 ship: ").strip().split(" ")))
	for i in b:
		ground1[i][a] = 1

	a = int(input("Enter Row for 1*3 ship: "))
	b = list(map(int,input("Enter columns for 1*3 ship: ").strip().split(" ")))
	for i in b:
		ground1[a][i] = 1

	a = int(input("Enter column for 4*1 ship: "))
	b = list(map(int,input("Enter Rows for 4*1 ship: ").strip().split(" ")))
	for i in b:
		ground1[i][a] = 1
	os.system('clear')
	print("Presentation of Ground 1")
	print("")
	for i in range(len(ground1)):
		print(ground1[i])
	print("")
	while(True):
		wanttocon = input("Press c to continue: " )
		if(wanttocon == "c") :
			break
		else:
			pass
	
	os.system('clear')

	print("Player 2 Form Your Ground: ")
	print("")
	a = int(input("Enter column for 2*1 ship: "))
	b = list(map(int,input("Enter Rows for 2*1 ship: ").strip().split(" ")))
	for i in b:
		ground2[i][a] = 1 

	a = int(input("Enter Row for 1*2 ship: "))
	b = list(map(int,input("Enter columns for 1*2 ship: ").strip().split(" ")))
	for i in b:
		ground2[a][i] = 1

	a = int(input("Enter column for 3*1 ship: "))
	b = list(map(int,input("Enter Rows for 3*1 ship: ").strip().split(" ")))
	for i in b:
		ground2[i][a] = 1

	a = int(input("Enter Row for 1*3 ship: "))
	b = list(map(int,input("Enter columns for 1*3 ship: ").strip().split(" ")))
	for i in b:
		ground2[a][i] = 1

	a = int(input("Enter column for 4*1 ship: "))
	b = list(map(int,input("Enter Rows for 4*1 ship: ").strip().split(" ")))
	for i in b:
		ground2[i][a] = 1
	os.system('clear')
	print("Presentation of Ground 2")
	print("")
	for i in range(len(ground1)):
		print(ground2[i])
	while(True):
		wanttocon = input("Press c to continue: " )
		if(wanttocon == "c") :
			_play_game()
		else:
			pass
def _is_won(ground):
	for i in range(len(ground)):
		for j in range(len(ground[i])):
			if(ground[i][j] == 1):
				return -1
	return 0 


				
			

def _play_game():
	turn = 1
	os.system('clear')
	while(True):
		os.system('clear')
		print("Player 1 Turn")
		print("M means Miss and H means Hit")	
		print("")
		while(turn == 1):
			for i in range(len(shownground1)):
				print(shownground1[i])
			print("")
			strikerow = int(input("Enter Your Attack Row: "))
			strikecol = int(input("Enter Your Attack Column: "))
			if ground2[strikerow][strikecol] == 1:
				os.system('clear')
				print("Player 1 Turn")
				print("M means Miss and H means Hit")	
				print("")
				shownground1[strikerow][strikecol] = "H"
				ground2[strikerow][strikecol] = 0
				check = _is_won(ground2)
				if check == 0:
					os.system('clear')
					print("congrats to player1 You have won the game!")
					print("")
					wantexit=input("press e to exit: ")
					if wantexit == "e":
						exit()
				else:
					pass
			elif ground2[strikerow][strikecol] == 0:
				os.system('clear')
				print("Player 1 Turn")
				print("M means Miss and H means Hit")	
				print("")
				shownground1[strikerow][strikecol] = "M"
				for i in range(len(shownground1)):
					print(shownground1[i])
				nextplayermove = input("Press m to move to Next Player: ")
				if nextplayermove == "m":
					turn *=-1
		os.system('clear')
		print("Player 2 Turn")
		print("M means Miss and H means Hit")	
		print("")
		while(turn == -1):
			for i in range(len(shownground2)):
				print(shownground2[i])
			print("")
			strikerow = int(input("Enter Your Attack Row: "))
			strikecol = int(input("Enter Your Attack Column: "))
			if ground1[strikerow][strikecol] == 1:
				os.system('clear')
				print("Player 2 Turn")
				print("M means Miss and H means Hit")	
				print("")
				shownground2[strikerow][strikecol] = "H"	
				ground1[strikerow][strikecol] = 0
				check = _is_won(ground1)
				if check == 0:
					os.system('clear')
					print("congrats to player2 You have won the game!")
					print("")
					wantexit=input("press e to exit: ")
					if wantexit == "e":
						exit()
					
				else:
					pass
			elif ground1[strikerow][strikecol] == 0:
				os.system('clear')
				print("Player 2 Turn")
				print("M means Miss and H means Hit")	
				print("")
				shownground2[strikerow][strikecol] = "M"
				for i in range(len(shownground2)):
					print(shownground2[i])
				nextplayermove = input("Press m to move to Next Player: ")
				if nextplayermove == "m":
					turn *=-1